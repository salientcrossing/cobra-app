CREATE DATABASE fortresstodo;

\c fortresstodo;

CREATE TABLE todo(
    todo_id SERIAL PRIMARY KEY,
    description VARCHAR(255)
);